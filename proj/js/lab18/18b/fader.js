$(document).ready(function () {
  $('li').css('margin', '10px');
  $('li').attr('id', 'uw');

  $('#p1 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(2000, function () {
      console.log("fadeout complete!")
    });
  });

  $('#p2 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut("slow", function () {
      console.log("fadeout complete!")
    });
  });

  $('#p3 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeTo("slow",0.4, function () {
      console.log("fadeout complete!")
    });
  });

  $('#p4 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(function () {
      console.log("fadeout complete!")
    });
  });
});