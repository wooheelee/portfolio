function calcCircleGeometries(radius) {
    var radius = prompt("Enter radius", "");

    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometires = [area+" cm"," "+circumference+" cm"," "+diameter+" cm"];

    document.write("Area: "+area+" cm", '<br>');
    document.write("Diameter: "+diameter+" cm", '<br>');
    document.write("Circumference: "+circumference+" cm", '<br>');
    document.write("Geometries: ["+geometires+"]");
    return geometires;
    }
