var array = [["BMW", "330i", "2019", "$40,750"],
["Mercedes-Benz", "C300", "2019", "$41,400"],
["Audi", "A4 2.0T", "2019", "$37,400"],
["Lexus", "Is300", "2019", "$38,560"],
["Tesla", "Model 3", "2019", "$35,000"]],
    table = document.getElementById("table");


for (var i = 1; i < table.rows.length; i++) {
    for (var j = 0; j < table.rows[i].cells.length; j++) {
        table.rows[i].cells[j].innerHTML = array[i - 1][j];
    }
}
