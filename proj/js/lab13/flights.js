var t1 = new Date ("November 18, 2019 16:02:00 GMT-0500");
var t2 = new Date ('November 18, 2019 17:09:00 GMT-0800');
var diff1 = t2.getTime()-t1.getTime();

var t3 = new Date ('November 18, 2019 15:11:00 GMT-0500');
var t4 = new Date ('November 18, 2019 17:12:00 GMT-0500');
var diff2 = t4.getTime()-t3.getTime();

var t5 = new Date ('November 18, 2019 15:06:00 GMT-0500');
var t6 = new Date ('November 18, 2019 16:28:00 GMT-0800');
var diff3 = t5.getTime()-t6.getTime();

var t7 = new Date ('November 18, 2019 14:09:00 GMT-0800');
var t8 = new Date ('November 18, 2019 16:32:00 GMT-0500');
var diff4 = t7.getTime()-t8.getTime();

var t9 = new Date ('November 18, 2019 14:02:00 GMT-0500');
var t10 = new Date ('November 18, 2019 16:17:00 GMT-0800');
var diff5 = t9.getTime()-t10.getTime();


var array = [["DAL1025", "A321", "Detroit Metropolitan Wayne County Airport (KDTW)", "San Diego Intl (KSAN)", t1 , t2],
["DAL1043", "B712", "John F. Kennedy International Airport (KJFK)", "Jacksonville Intl (KJAX)", t3 , t4],
["DAL106", "A319", "Minneapolis−Saint Paul International Airport (KMSP)", "John F. Kennedy International Airport (KJFK)", t5 , t6],
["DAL1060", "B739", "Fort Lauderdale Intl (KFLL)", "Detroit Metropolitan Wayne County Airport (KDTW)", t7 , t8],
["DAL1063", "B712", "St Louis Lambert Intl (KSTL)", "Detroit Metropolitan Wayne County Airport (KDTW)", t9 , t10]],
    table = document.getElementById("table");


for (var i = 1; i < table.rows.length; i++) {
    for (var j = 0; j < table.rows[i].cells.length; j++) {
        table.rows[i].cells[j].innerHTML = array[i - 1][j];
    }
}
