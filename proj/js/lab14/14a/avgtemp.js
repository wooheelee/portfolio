var data = [["Fri", "82", "55"],
["Sat", "75", "52"],
["Sun", "69", "52"],
["Mon", "69", "48"],
["Tue", "68", "51"],
],
table = document.getElementById("table");

for (var i = 1; i < table.rows.length; i++) {
    for (var j = 0; j < table.rows[i].cells.length; j++) {
        table.rows[i].cells[j].innerHTML = data[i - 1][j];
    }
}

var wx_data = [
    {
        day: "fri",
        hi: 82,
        lo: 55
    },
    {
        day: "sat",
        hi: 75,
        lo: 52
    },
    {
        day: "sun",
        hi: 69,
        lo: 52
    },
    {
        day: "mon",
        hi: 69,
        lo: 48
    },
    {
        day: "tue",
        hi: 68,
        lo: 51
    }
];

var sum_Hi = wx_data.reduce(function (a, b) {
    return a + b.hi;
}, 0);

var sum_Lo = wx_data.reduce(function (a, b) {
    return a + b.lo;
}, 0);

var avg_Hi = sum_Hi / wx_data.length;
var avg_LO = sum_Lo / wx_data.length;

document.write("High temperatures average: " + avg_Hi);
document.write('<br/>');
document.write("Low temperatures average: " + avg_LO);